#!/usr/bin/env python3

import os
from sys import exit
import csv
from prettytable import PrettyTable


def build_data_table():
    file_number = 1  # номер в названии файла
    flight_counter = 1  # нумерация строк

    # строим поля таблицы
    data_table = PrettyTable()
    data_table.field_names = ["№", "SRS", "CLB", "CLB FL100", "CS FL350", "MCS", "DS", "V stall", "G factor", "MB"]

    while os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)) + "/data", "raw_data_" + str(file_number) + ".csv")):
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)) + "/data", "raw_data_" + str(file_number) + ".csv"), "r", encoding='utf-8') as id_file:
            data = csv.reader(id_file, delimiter=';', quotechar='"')
            data_read = [row for row in data]
        id_file.close()

        # заполняем таблицу данными
        data_table.add_row([flight_counter, data_read[0][2], data_read[1][2], data_read[2][2], data_read[3][2], data_read[4][2], data_read[5][2], data_read[6][2], data_read[7][2], data_read[8][2]])
        flight_counter += 1
        file_number += 1
    print(data_table)


# функция открытия файла получения данных и отпраки в обработчики
def open_read_result():
    file_number = 1  # номер в названии файла
    flight_counter = 1  # нумерация строк

    # списки в которые юудут записываться данные
    speed_reference_system_result = []
    climb_speed_result = []
    climb_above_fl100_result = []
    cruise_speed_at_fl350_result = []
    minimum_cruise_speed_result = []
    descending_speed_result = []
    vstall_result = []
    g_factor_result = []
    maximum_bank_result = []

    while os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)) + "/data", "raw_data_" + str(file_number) + ".csv")):
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)) + "/data", "raw_data_" + str(file_number) + ".csv"), "r", encoding='utf-8') as id_file:
            data = csv.reader(id_file, delimiter=';', quotechar='"')
            data_read = [row for row in data]
            id_file.close()
            # отправляем данные в обработчики, результат дозаписываем в список
            speed_reference_system_result.append(speed_reference_system(data_read[0][2]))
            climb_speed_result.append(speed_reference_system(data_read[1][2]))
            climb_above_fl100_result.append(climb_above_fl100(data_read[2][2]))
            cruise_speed_at_fl350_result.append(cruise_speed_at_fl350(data_read[3][2]))
            minimum_cruise_speed_result.append(minimum_cruise_speed(data_read[4][2]))
            descending_speed_result.append(descending_speed(data_read[5][2]))
            vstall_result.append(vstall(data_read[6][2]))
            g_factor_result.append(g_factor(data_read[7][2]))
            maximum_bank_result.append(maximum_bank(data_read[8][2]))
            file_number += 1
    return [speed_reference_system_result, climb_speed_result, climb_above_fl100_result, cruise_speed_at_fl350_result,
            minimum_cruise_speed_result, descending_speed_result, vstall_result, g_factor_result, maximum_bank_result]


# 1. Speed reference system
def speed_reference_system(value):
    value = int(value)
    r = range(160, 171)
    if value > 170:
        return str('normal')
    elif value in r:
        return str('low')
    else:
        return str('danger')


# 2. Climb speed
def climb_speed(value):
    value = int(value)
    r = range(250, 261)
    if value > 260:
        return str('normal')
    elif value in r:
        return str('low')
    else:
        return str('danger')


# 3. Climb above FL100
def climb_above_fl100(value):
    value = int(value)
    r = range(300, 316)
    if value > 315:
        return str('normal')
    elif value in r:
        return str('low')
    else:
        return str('danger')


# 4. Cruise speed at FL350
def cruise_speed_at_fl350(value):
    value = int(value)
    r = range(240, 251)
    if value > 250:
        return str('normal')
    elif value in r:
        return str('low')
    else:
        return str('danger')


# 5. Minimum cruise speed
def minimum_cruise_speed(value):
    value = float(value)
    if value > 0.73:
        return str('normal')
    elif 0.72 <= value <= 0.73:
        return str('low')
    else:
        return str('danger')


# 6. Descending speed
def descending_speed(value):
    value = int(value)
    r = range(150, 161)
    if value > 160:
        return str('normal')
    elif value in r:
        return str('low')
    else:
        return str('danger')


# 7. Vstall
def vstall(value):
    value = int(value)
    if value == 0:
        return str('normal')
    elif value == 1:
        return str('danger')


# 8. G factor
def g_factor(value):
    value = float(value)
    if value <= 1.7:
        return str('normal')
    elif value > 1.7:
        return str('danger')


# 9. Maximum bank
def maximum_bank(value):
    value = int(value)
    if value <= 33:
        return str('normal')
    else:
        return str('danger')


# Рассчёт процентов 1
def percent_1(f_list):
    values_count = len(f_list)
    bad = 0  # количество нехороших показателей
    if f_list.count("danger") > 0:
        return 1000
    for i in f_list:
        if i == 'low':
            bad += 1
    result = round(bad*100/values_count, 2)
    return result


# Обработка ОК или нет 1
def ok_or_not_1(f_list):
    if f_list.count("danger") > 0:
        return 1000
    else:
        return 200


def condition_checker(name, data, tester):
    if tester == 'percent_1':
        if percent_1(data) == 1000:
            summary_table.add_row([name + ':', '', 'Грубое нарушение эксплуатационных ограничений'])
        elif 70 <= percent_1(data) < 75:
            summary_table.add_row([name + ':', percent_1(data), 'Рекомендуется беседа с КВС эскадрильи'])
        elif percent_1(data) > 74:
            summary_table.add_row([name + ':', percent_1(data), 'Рекомендуется дополнительная тренажерная подготовка'])
        else:
            summary_table.add_row([name + ':', percent_1(data), 'OK'])
    elif tester == 'ok_or_not_1_1':
        if ok_or_not_1(data) == 1000:
            summary_table.add_row([name + ':', '', 'Грубое нарушение эксплуатационных ограничений'])
        elif ok_or_not_1(data) == 200:
            summary_table.add_row([name + ':', 'normal', 'OK'])
    elif tester == 'ok_or_not_1_2':
        if ok_or_not_1(data) == 1000:
            summary_table.add_row([name + ':', 'is present', 'Грубое нарушение эксплуатационных ограничений'])
        elif ok_or_not_1(data) == 200:
            summary_table.add_row([name + ':', 'not present', 'OK'])
    else:
        summary_table.add_row(['Переданы не все поля для расчета', 'Переданы не все поля для расчета', 'Переданы не все поля для расчета'])


# Основная логика
if __name__ == '__main__':
    input('''Добро пожаловать в программу для анализа параметров полёта.
    
    Загрузите данные в формате csv в директорию data
---------------------------------------------------------    
    Нажмите Enter для анализа данных
    ''')
    try:
        build_data_table()
        print('''

            Отчёт по анализу данных:
        --------------------------------
            ''')

        # строим поля таблицы
        summary_table = PrettyTable()
        summary_table.field_names = ["Показатель", "Значение (%)", "Рекомендации"]

        # 1. Speed reference system
        condition_checker('Speed reference system', open_read_result()[0], 'percent_1')

        # 2. Climb speed
        condition_checker('Climb speed', open_read_result()[1], 'percent_1')

        # 3. Climb above FL100
        condition_checker('Climb above FL100', open_read_result()[2], 'percent_1')

        # 4. Cruise speed at FL350
        condition_checker('Cruise speed at FL350', open_read_result()[3], 'percent_1')

        # 5. Minimum cruise speed
        condition_checker('Minimum cruise speed', open_read_result()[4], 'percent_1')

        # 6. Descending speed
        condition_checker('Descending speed', open_read_result()[5], 'percent_1')

        # 7. Vstall
        condition_checker('Vstall', open_read_result()[6], 'ok_or_not_1_2')

        # 8. G factor
        condition_checker('G factor', open_read_result()[7], 'ok_or_not_1_1')

        # 9. Maximum bank
        condition_checker('Maximum bank', open_read_result()[8], 'ok_or_not_1_1')

        print(summary_table)
        input('\nНажмите Enter для завершения работы')
    except IOError:
        print('Выход из приложения')
        exit(0)
